package uk.mattr.intentreceiver;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.amazonaws.services.iot.client.AWSIotMqttClient;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Handler;

/**
 * Created by mattr on 06/05/17.
 */
public class Receiver extends IntentService {
    public Receiver () {
        super("name");
    }

    @Override
    public void onHandleIntent (Intent intent) {
        JSONObject payload = new JSONObject();

        if(intent.getAction() != null) {
            addToObject(payload, "Action", intent.getAction());
        }
        if(intent.getDataString() != null) {
            addToObject(payload, "Data", intent.getDataString());
        }
        if(intent.getPackage() != null) {
            addToObject(payload, "Package", intent.getPackage());
        }
        if(intent.getScheme() != null) {
            addToObject(payload, "Scheme", intent.getScheme());
        }
        if(intent.getType() != null) {
            addToObject(payload, "Type", intent.getType());
        }
        if(intent.getExtras() != null) {
            JSONArray extras = new JSONArray();

            for (String key : intent.getExtras().keySet()) {
                JSONObject extra = new JSONObject();

                addToObject(extra, "Key", key);
                addToObject(extra, "Value", intent.getStringExtra(key));
                extras.put(extra);
            }
            try {
                payload.put("Extras", extras);
            }
            catch (Exception ex) {
                System.out.print(ex.getMessage());
            }
        }

        try {
            SettingsActivity.publishMessage(payload.toString());
            System.out.print("SENT");
        }
        catch(Exception ex) {
            System.out.print(ex.getMessage());
        }

    }

    private void addToObject (JSONObject json, String key, String value) {
        try {
            json.put(key, value);
        }
        catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }
}
