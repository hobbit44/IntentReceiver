package uk.mattr.intentreceiver;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.view.MenuItem;
import android.widget.Toast;


import com.amazonaws.services.iot.client.AWSIotConnectionStatus;
import com.amazonaws.services.iot.client.AWSIotMqttClient;

import java.util.List;
import java.util.UUID;


public class SettingsActivity extends AppCompatPreferenceActivity {

    public static AWSIotMqttClient CLIENT;

    public SettingsActivity () {
    }

    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();

        SharedPreferences prefs = getSharedPreferences("uk.mattr.IntentReceiver", Context.MODE_PRIVATE);

        String clientEndpoint = prefs.getString("uk.mattr.IntentReceiver.awsEndpoint", "");
        String clientId = new UUID(20, 10).toString();
        String awsAccessKeyId = prefs.getString("uk.mattr.IntentReceiver.awsAccessKey", "");
        String awsSecretAccessKey = prefs.getString("uk.mattr.IntentReceiver.awsSecretKey", "");

        if (!awsAccessKeyId.equals("") || !awsSecretAccessKey.equals("")) {
            CLIENT = new AWSIotMqttClient(clientEndpoint, clientId, awsAccessKeyId, awsSecretAccessKey);

            CLIENT.setMaxConnectionRetries(3);
            CLIENT.setMaxRetryDelay(1);
            if (CLIENT.getConnectionStatus() == AWSIotConnectionStatus.DISCONNECTED) {
                try {
                    CLIENT.connect();
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    Toast.makeText(this, "NOT CONNECTED", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public static void publishMessage(String payload) {
        try {
            CLIENT.publish("Intent", payload);
        }
        catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            SharedPreferences prefs = getActivity().getSharedPreferences("uk.mattr.IntentReceiver", Context.MODE_PRIVATE);

            Preference awsEndpoint = (Preference) findPreference("aws_endpoint");
            awsEndpoint.setSummary(prefs.getString("uk.mattr.IntentReceiver.awsEndpoint", ""));

            Preference awsAccessKey = (Preference) findPreference("aws_access_key");
            awsAccessKey.setSummary(prefs.getString("uk.mattr.IntentReceiver.awsAccessKey", ""));

            Preference awsSecretKey = (Preference) findPreference("aws_secret_key");

            awsEndpoint.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SharedPreferences prefs = getActivity().getSharedPreferences("uk.mattr.IntentReceiver", Context.MODE_PRIVATE);
                    prefs.edit().putString("uk.mattr.IntentReceiver.awsEndpoint", newValue.toString()).apply();
                    preference.setSummary(newValue.toString());
                    return true;
                }
            });

            awsAccessKey.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SharedPreferences prefs = getActivity().getSharedPreferences("uk.mattr.IntentReceiver", Context.MODE_PRIVATE);
                    prefs.edit().putString("uk.mattr.IntentReceiver.awsAccessKey", newValue.toString()).apply();
                    preference.setSummary(newValue.toString());
                    return true;
                }
            });

            awsSecretKey.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SharedPreferences prefs = getActivity().getSharedPreferences("uk.mattr.IntentReceiver", Context.MODE_PRIVATE);
                    prefs.edit().putString("uk.mattr.IntentReceiver.awsSecretKey", newValue.toString()).apply();
                    preference.setSummary("");
                    return true;
                }
            });

        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
}
