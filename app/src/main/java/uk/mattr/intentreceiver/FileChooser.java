package uk.mattr.intentreceiver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by mattr on 06/05/17.
 */
public class FileChooser extends Activity {

    static final int PICKFILE_REQUEST_CODE = 1;

    public void onCreate (Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == PICKFILE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String uri = intent.getDataString();
                System.out.println(uri);
            }
        }
    }
}
